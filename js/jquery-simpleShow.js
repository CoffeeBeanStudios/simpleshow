$(function() {
	var DEBUG = false;
    var version = '1.1';
	var slideshow;
	
    $.fn.simpleShow = function(options) {
        //var opts = $.extend({}, $.fn.nailthumb.defaults, options);
		
		
        return this.each(function() {
            var $this = $(this);
			
            new SlideShow($this, options);
			//prepareShow($this);
        });
    };
});

function SlideShow(element, options){
	var self=element;
	var slides=new Array();
	var index=0;
	var slideCount=0;
	var interval=5000;
	var timer;
	var mutex=0;
	var useMenu=true;
	
	// UI elements
	var menu;
	var nextBtn;
	var prevBtn;
	
	init(options);
	
	function init(options){
		showLoadingBar();
		
		// handle options
		if (options){
			if ('width' in options){
				var width = options['width'];
				self.css('width', width);
			}
			
			if ('interval' in options){
				interval=options['interval'];
			}
			
			if ('showMenu' in options){
				useMenu=options['showMenu'];
			}
			
			// this needs to be the last option parsed
			if (options['ajax']){
				loadAjax(options['ajax']);
				//we'll finish preparing after the content is loaded
				return;
			}			
		}
		
		prepare();
	}
	
	function prepare(){
		// prepare each slide
		var slides = self.children('.simpleShow_Slide');
		slides.each(prepareSlide);
		
		// place the buttons and indicators
		setupUI();
		
		// should be finished loading
		hideLoadingBar();
		
		// start the swap timer
		startTimer();	
	}
	
	// prepares each slide in a slide show
	function prepareSlide(index, element){	
		$this = $(this);
		
		// make the first visible
		if (index == 0){
			$this.css('left', '0');
			$this.css('top', '0');
		}
		// and the rest off the screen
		else{
			$this.css('left', '100%');
			$this.css('top', '0');
		}
		
		$this.css('visibility', 'visible');
		$this.css('position', 'absolute');		
		$this.click(slideClick);
		
		slides.push($this);
		slideCount++;
		
		// any other preparations
	}
	
	function slideClick(e){
		if (useMenu){
			e.preventDefault();
			showMenu();
		}
	}
	
	// fires each time we want to swap a picture
	function swapPicture(){
		goForward();
	}
	
	function goForward(){
		if (mutex>0){return;}
		mutex++;
	
		var nextIndex = index+1;
		if (nextIndex>slideCount-1){nextIndex=0;}	
		
		// animate current one off the screen
		slides[index].animate({left: '-100%'}, 
							  {duration: 1000,
								queue:false,
								complete: animationFinished});
								
		// simultaneously animate the next one onto screen
		repositionSlideRight(slides[nextIndex]);
		slides[nextIndex].animate({left: '0px'}, 
							  {duration: 1000,
								queue:false});
		
		
		if (index<slideCount-1){
			index++;
		}
		else{
			index=0;
		}
		
		// reset the interval
		startTimer();
	}
	
	function goBackward(){
		if (mutex>0){return;}
		mutex++;
	
		var nextIndex = index-1;
		if (nextIndex<0){nextIndex=slideCount-1;}	
		
		// animate current one off the screen
		slides[index].animate({left: '100%'}, 
							  {duration: 1000,
								queue:false,
								complete: animationFinished});
								
		// simultaneously animate the next one onto screen
		repositionSlideLeft(slides[nextIndex]);
		slides[nextIndex].animate({left: '0px'}, 
							  {duration: 1000,
								queue:false});
		
		
		if (index>0){
			index--;
		}
		else{
			index=slideCount-1;
		}
		
		// reset the interval
		startTimer();
	}
	
	function animationFinished(slide){
		mutex--;
	}
	
	function repositionSlideRight(slide){
		//var $this = $(this);		
		slide.css('left', '100%');		
	}
	
	function repositionSlideLeft(slide){
		//var $this = $(this);		
		slide.css('left', '-100%');		
	}
	
	function stopTimer(){
		clearInterval(timer);
	}
	
	function startTimer(){
		stopTimer();
		timer = setInterval(swapPicture, interval);
	}
	
	function setupUI(){

		//add previous button
		self.append('<img id="simpleShow_previous" class="simpleShow_Navigation" src="img/arrow-prev.png"></img>');		
		prevBtn = $('#simpleShow_previous');
		prevBtn.click(goBackward);
		
		// add next button
		self.append('<img id="simpleShow_next" class="simpleShow_Navigation" src="img/arrow-next.png"></img>');		
		nextBtn = $('#simpleShow_next');
		nextBtn.click(goForward);
		
		// add the menu and hide it
		self.append('<div id="simpleShow_menu">' +
					'	<div id="closeButton">x</div>' +
					'	<div id="viewButton" class="simpleShow_button">View Post</div>' +
					'	<div id="shareButton" class="simpleShow_button">E-mail to Friend</div>' +
					'	<div id="downloadButton" class="simpleShow_button">Download Image</div>' +
					'</div>');
		menu = $('#simpleShow_menu');	
		
		// attach menu click handlers
		menu.children('#viewButton').click(viewClicked);
		menu.children('#shareButton').click(shareClicked);
		menu.children('#downloadButton').click(downloadClicked);	
		menu.children('#closeButton').click(closeClicked);
	}	
	
	function loadAjax(ajaxURL){		
		$.ajax({
		  url: ajaxURL,
		  success: ajaxLoaded
		});
	}
	
	function ajaxLoaded(data){
		var json = jQuery.parseJSON(data);	
		var json_slides = json.slides;
		
		// for each image build the dom 
		var out;
		json_slides.forEach(function(e, i){
			out = '<div class="simpleShow_Slide">' +
					'	<a href="' + e.link + '">' +
					'		<img src="' + e.src + '">' +
					'		<div>' +
					'			<span class="simpleShow_title">' + e.title + '</span><br>' +
					'			<span class="simpleShow_caption">' + e.caption + '</span>' +
					'		</div>' +
					'	</a>' +
					'</div>';
			
			self.append(out);
		});
					
		// then call prepare to parse the dom and convert to slides
		prepare();
	}
	
	function showLoadingBar(){
		self.append('<img class="simpleShow_loading" src="img/ajax-loader.gif"/>');
	}
	
	function hideLoadingBar(){
		$('.simpleShow_loading').css('visibility', 'hidden');
	}
	
	function viewClicked(e){
		console.log("view");
		var href = slides[index].children('a').attr('href');
		window.location = href;
		//console.log(slides[index].children('a').attr('href'));
	}
	
	function showMenu(){
		stopTimer();
		menu.css('visibility', 'visible');		
	}
	
	function hideMenu(){
		menu.css('visibility', 'hidden');
		startTimer();
	}
	
	function shareClicked(e){
		var href = slides[index].children('a').attr('href');
	
		var subject= "Interesting Post";
        var body = "I thought you might find this post interesting:\r\n\r\n<";
        body += href;
        body += ">";
        var uri = "mailto:?subject=";
        uri += encodeURIComponent(subject);
        uri += "&body=";
        uri += encodeURIComponent(body);
        window.open(uri);
		
		menu.css('visibility', 'hidden');
		hideMenu();
	}
	
	function downloadClicked(e){
		var src = slides[index].children('a').children('img').attr('src');
		console.log(src);
		window.open(src);
		
		hideMenu();
	}
	
	function closeClicked(e){
		hideMenu();
	}
	
}